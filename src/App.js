import React from 'react';
import './App.css';
import {
  Route,
  NavLink,
  Switch,
  BrowserRouter
} from "react-router-dom";

import Root from "./components/_Root/Root.jsx"

function App() {
  return (
    <div className="App">
        <Root />
    </div>
  );
}

export default App;
