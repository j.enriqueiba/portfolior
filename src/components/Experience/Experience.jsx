import React, { Component } from 'react'
import WebIcon from '@material-ui/icons/Web';

import './style.css'

class Experience extends Component {
  render() {
    return (
      <div id="body" className="final-body-container" align="center">
        <div className="experience-title-container">
          <h1 className="page-title">EXPERIENCE</h1>
        </div>

        <div className="page-text">
          <p>- Web Applications Developer / Data Automatization Engineer for <b>Textron Aviation</b></p>
        </div>
        <div className="page-text">
          <p>- Web Developer for <b>Instituto Tecnologico de la Laguna</b>  <a className="link" href="http://itlalaguna.edu.mx/nuevo" target="blank">itlalaguna.edu.mx/nuevo</a></p>
        </div>
      </div>
    )
  }
}



export default Experience