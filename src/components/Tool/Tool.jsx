import React, { Component } from 'react'
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import './style.css'

class Tool extends Component {
	render() {

		const useStylesBootstrap = makeStyles(theme => ({
			arrow: {
				color: "#0006FF"
			},
			tooltip: {
				backgroundColor: "#0006FF",
			}
		}));

		const useStylesBootstrapDark = makeStyles(theme => ({
			arrow: {
				color: "#260101"
			},
			tooltip: {
				backgroundColor: "#260101",
			}
		}));

		function BootstrapTooltip(props) {
			const classes = useStylesBootstrap();

			return <Tooltip arrow classes={classes} {...props} />;
		}

		function BootstrapTooltipDark(props) {
			const classes = useStylesBootstrapDark();

			return <Tooltip arrow classes={classes} {...props} />;
		}

		return (
			<div id="body" className="body-container" align="center">
				<div className="tool-title-container">
					<h1 className="page-title">MY SKILLS</h1>
				</div>

				<div className="page-text" style={{ textAlign: "center" }}>
					<p>Platforms I've developed for</p>

					<BootstrapTooltip title="Android" aria-label="add">
						<i className="devicon-android-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="macOS & iOS" aria-label="add">
						<i className="devicon-apple-original tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="Windows" aria-label="add">
						<i className="devicon-windows8-original tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="Linux" aria-label="add">
						<i className="devicon-linux-plain tool-icon"></i>
					</BootstrapTooltip>

					<hr />

					<p>Languages I've developed in</p>

					<BootstrapTooltip title="C++" aria-label="add">
						<i className="devicon-cplusplus-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="C#" aria-label="add">
						<i className="devicon-csharp-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="HTML 5" aria-label="add">
						<i className="devicon-html5-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="Javascript" aria-label="add">
						<i className="devicon-javascript-plain tool-icon"></i>
					</BootstrapTooltip>

					<p></p>

					<BootstrapTooltip title="Java" aria-label="add">
						<i className="devicon-java-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="PHP" aria-label="add">
						<i className="devicon-php-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="Python" aria-label="add">
						<i className="devicon-python-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="SQL" aria-label="add">
						<i className="devicon-mysql-plain tool-icon"></i>
					</BootstrapTooltip>

					<hr />

					<p>Frameworks I've developed with</p>

					<BootstrapTooltip title="Bootstrap" aria-label="add">
						<i className="devicon-bootstrap-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title=".NET" aria-label="add">
						<i className="devicon-dot-net-plain tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="ExpressJS" aria-label="add">
						<i className="devicon-express-original tool-icon"></i>
					</BootstrapTooltip>
					<BootstrapTooltip title="NodeJS" aria-label="add">
						<i className="devicon-nodejs-plain tool-icon"></i>
					</BootstrapTooltip>

					<p></p>

					<BootstrapTooltipDark title="React" aria-label="add">
						<i className="devicon-react-original tool-icon"></i>
					</BootstrapTooltipDark>
					<BootstrapTooltipDark title="SASS" aria-label="add">
						<i className="devicon-sass-original  tool-icon"></i>
					</BootstrapTooltipDark>
				</div>

				<div className="section-text" style={{marginTop: "0px"}}>
					<p style={{textAlign: "center"}}>Also experienced in <b>Adobe Creative Suite</b> and <b>Microsoft Office</b> software!</p>
				</div>
			</div>
		)
	}
}



export default Tool