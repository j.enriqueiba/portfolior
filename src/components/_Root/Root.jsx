import React, { Component } from 'react'
import SideBar from 'react-fixed-sidebar';
import Grid from '@material-ui/core/Grid';

import { Route, NavLink, Switch, BrowserRouter } from "react-router-dom";
import ScrollableAnchor from 'react-scrollable-anchor'

import PersonRoundedIcon from '@material-ui/icons/PersonRounded';
import BuildRoundedIcon from '@material-ui/icons/BuildRounded';
import LanguageRoundedIcon from '@material-ui/icons/LanguageRounded';

import About from "../About/About.jsx"
import Tools from "../Tool/Tool.jsx"
import Experience from "../Experience/Experience.jsx"

import './style.css'

class Root extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		this.sidebar.open()
	}

	render() {
		return (
			<div>
				<SideBar
					ref={sidebar => this.sidebar = sidebar}
					width="75px"
					className="sidebar-container"
					style={{backgroundColor: "#0433BF !important"}}
				>
					<div style={{ height: "120vh" }}>
						<Grid container direction="row" className="menu-container">
							<a href="#about" className="icon-container">
								<Grid xs={12} >
									<PersonRoundedIcon className="sidebar-icon"/>
								</Grid>
							</a>

							<a href="#tools" className="icon-container">
								<Grid xs={12} className="icon-container">
									<BuildRoundedIcon className="sidebar-icon"/>
								</Grid>
							</a>

							<a href="#experience" className="icon-container">
								<Grid xs={12} className="icon-container">
									<LanguageRoundedIcon className="sidebar-icon"/>
								</Grid>
							</a>
						</Grid>
					</div>
				</SideBar>

				<div id="content">
					<ScrollableAnchor id={'about'}>
						<About />
					</ScrollableAnchor>

					<ScrollableAnchor id={'tools'}>
						<Tools />
					</ScrollableAnchor>

					<ScrollableAnchor id={'experience'}>
						<Experience />
					</ScrollableAnchor>
				</div>

			<div className="footer-container">
				<p style={{paddingBottom: "20vh"}}>Copyright 2020. Website designed and programmed by Enrique Ibarra.</p>
			</div>

			</div>
		)
	}
}



export default Root