import React, { Component } from 'react'
import About from "./About/About.jsx"
import Tools from "./Tool/Tool.jsx"


class Container extends Component {
  render() {
    return (
      <div>
        <About />
        <Tools />
      </div>
    )
  }
}


export default Container