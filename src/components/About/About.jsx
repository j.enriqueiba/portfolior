import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';

class About extends Component {
  render() {
    return (
      <div id="body" className="first-container" align="center">
        <h1 className="title" style={{marginBottom: "0px"}}>ENRIQUE IBARRA</h1>
        <p style={{marginTop: "5px"}}>j.enriqueiba@gmail.com / (871) 316-4715</p>
        <p style={{marginTop: "5px"}}>Chihuahua, Chih. Mexico.</p>
        <div className="page-title-container" style={{marginTop: "3em"}}>
          <h1 className="page-title">ABOUT ME</h1>
        </div>

        <div className="page-text">
         <p> I'm a bilingual software engineer with a passion for effective problem solving and impeccable design.</p>
					</div>

        <div className="section-text" style={{marginTop: "0px"}}>
          <p><b>Education</b> </p>
          <p>Software And Systems Engineering at Instituto Tecnologico de la Laguna (2015-2020)</p>

          <p><b>Languages</b> </p>
          <p>English: High proficiency (TOEFL 2019)</p>
          <p>Spanish: Native language</p>
        </div>

      </div>
    )
  }
}



export default About